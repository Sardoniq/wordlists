#!/usr/bin/env bash

# Download and extract Wordnet database files
wget -qO- https://wordnetcode.princeton.edu/wn3.1.dict.tar.gz | tar -xz

# Keep only relevant files
mv dict/index.[^s]* .
rm -rf dict

# Extract wordlists by parts of speech
grep -Po '^.*?\s' index.noun | tr -d ' ' > all_nouns.txt
grep -Po '^.*?\s' index.adj | tr -d ' ' > all_adjectives.txt
grep -Po '^.*?\s' index.adv | tr -d ' ' > all_adverbs.txt
grep -Po '^.*?\s' index.verb | tr -d ' ' > all_verbs.txt
rm -rf index.*

# Download the 20k most common words
curl -s http://norvig.com/ngrams/count_1w.txt | head -n 20000 | tr -d "[:blank:]" | sed 's/[0-9]*//g' > 20k.txt

# Keep only words that are in the 20k most common
grep -Fxf all_adjectives.txt 20k.txt > adjectives.txt
grep -Fxf all_nouns.txt 20k.txt > nouns.txt
grep -Fxf all_adverbs.txt 20k.txt > adverbs.txt
grep -Fxf all_verbs.txt 20k.txt > verbs.txt

# Remove old files
rm -rf all_*.txt
rm -rf 20k.txt
