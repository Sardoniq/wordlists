# wordlists

## Description
A set of wordlists separated by part of speech, consisting only of common words.

## Usage
Download the wordlists directly, download and execute the shell script to download them automatically on a Linux system, or execute the commands below.

**NOTE: Please make sure to run shell script in an empty folder, as otherwise it may delete local files!**

## Manually get wordlists
1. Download and extract Wordnet database files:
```
wget -qO- https://wordnetcode.princeton.edu/wn3.1.dict.tar.gz | tar -xz
```

2. Keep only relevant files
```
mv dict/index.[^s]* .
rm -rf dict
```

3. Extract wordlists
```
grep -Po '^.*?\s' index.noun | tr -d ' ' > all_nouns.txt
grep -Po '^.*?\s' index.adj | tr -d ' ' > all_adjectives.txt
grep -Po '^.*?\s' index.adv | tr -d ' ' > all_adverbs.txt
grep -Po '^.*?\s' index.verb | tr -d ' ' > all_verbs.txt
rm -rf index.*
```

4. Download the 20k most common words
```
curl -s http://norvig.com/ngrams/count_1w.txt | head -n 20000 | tr -d "[:blank:]" | sed 's/[0-9]*//g' > 20k.txt
```
5. Keep only words that are also in the 20k most common
```
grep -Fxf all_adjectives.txt 20k.txt > adjectives.txt
grep -Fxf all_nouns.txt 20k.txt > nouns.txt
grep -Fxf all_adverbs.txt 20k.txt > adverbs.txt
grep -Fxf all_verbs.txt 20k.txt > verbs.txt
```
6. Remove old files
```
rm -rf all_*.txt
rm -rf 20k.txt
```

## Authors and acknowledgment
- Written by Sardoniq
- Original wordlists from Wordnet (Princeton University "About WordNet." WordNet. Princeton University. 2010.)
- List of most frequent words taken from [Peter Norvig](https://norvig.com/ngrams/count_1w.txt), originally scraped from the Google Web Trillion Word Corpus, as [described](http://googleresearch.blogspot.com/2006/08/all-our-n-gram-are-belong-to-you.html) by Thorsten Brants and Alex Franz, and distributed by the Linguistic Data Consortium. 
